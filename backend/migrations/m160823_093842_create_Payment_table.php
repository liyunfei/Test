<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Handles the creation for table `payment`.
 */
class m160823_093842_create_Payment_table extends Migration {

    /**
     * @inheritdoc
     */
    public function up() {
        $tableName = '{{%payment}}';
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        $this->createTable($tableName, [
            'id' => $this->primaryKey(),
            'mch_appid' => Schema::TYPE_STRING . "(20) NULL COMMENT '公众账号appid'",
            'mchid' => Schema::TYPE_STRING . "(32) NULL COMMENT '商户号'",
            'device_info' => Schema::TYPE_STRING . "(32) NULL COMMENT '设备号'",
            'nonce_str' => Schema::TYPE_STRING . "(32) NULL COMMENT '随机字符串'",
            'sign' => Schema::TYPE_STRING . "(32) NULL COMMENT '签名'",
            'partner_trade_no' => Schema::TYPE_STRING . "(32) NULL COMMENT '商户订单号'",
            'openid' => Schema::TYPE_STRING . "(32) NULL COMMENT '用户openid'",
            'check_name' => Schema::TYPE_STRING . "(12) NULL COMMENT '校验用户姓名选项'",
            're_user_name' => Schema::TYPE_STRING . "(32) NULL COMMENT '收款用户姓名'",
            'amount' => Schema::TYPE_INTEGER . "(7) NULL COMMENT '金额'",
            'desc' => Schema::TYPE_STRING . "(128) NULL COMMENT '企业付款描述信息'",
            'spbill_create_ip' => Schema::TYPE_STRING . "(32) NULL COMMENT 'Ip地址'",
            'return_code' => Schema::TYPE_STRING . "(16) NULL COMMENT '返回状态码'",
            'return_msg' => Schema::TYPE_STRING . "(128) NULL COMMENT '业务结果'",
            'result_code' => Schema::TYPE_STRING . "(16) NULL COMMENT '业务结果'",
            'err_code' => Schema::TYPE_STRING . "(32) NULL COMMENT '错误代码'",
            'err_code_des' => Schema::TYPE_STRING . "(128) NULL COMMENT'错误代码描述'",
            'payment_no' => Schema::TYPE_STRING . "(32) NULL COMMENT '微信订单号'",
            'payment_time' => Schema::TYPE_STRING . "(32) NULL COMMENT'微信支付成功时间'"
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down() {
        $this->dropTable('{{%payment}}');
    }

}
