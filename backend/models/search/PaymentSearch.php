<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Payment;

/**
 * PaymentSearch represents the model behind the search form about `backend\models\Payment`.
 */
class PaymentSearch extends Payment
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'amount'], 'integer'],
            [['mch_appid', 'mchid', 'device_info', 'nonce_str', 'sign', 'partner_trade_no', 'openid', 'check_name', 're_user_name', 'desc', 'spbill_create_ip', 'return_code', 'return_msg', 'result_code', 'err_code', 'err_code_des', 'payment_no', 'payment_time'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Payment::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'amount' => $this->amount,
        ]);

        $query->andFilterWhere(['like', 'mch_appid', $this->mch_appid])
            ->andFilterWhere(['like', 'mchid', $this->mchid])
            ->andFilterWhere(['like', 'device_info', $this->device_info])
            ->andFilterWhere(['like', 'nonce_str', $this->nonce_str])
            ->andFilterWhere(['like', 'sign', $this->sign])
            ->andFilterWhere(['like', 'partner_trade_no', $this->partner_trade_no])
            ->andFilterWhere(['like', 'openid', $this->openid])
            ->andFilterWhere(['like', 'check_name', $this->check_name])
            ->andFilterWhere(['like', 're_user_name', $this->re_user_name])
            ->andFilterWhere(['like', 'desc', $this->desc])
            ->andFilterWhere(['like', 'spbill_create_ip', $this->spbill_create_ip])
            ->andFilterWhere(['like', 'return_code', $this->return_code])
            ->andFilterWhere(['like', 'return_msg', $this->return_msg])
            ->andFilterWhere(['like', 'result_code', $this->result_code])
            ->andFilterWhere(['like', 'err_code', $this->err_code])
            ->andFilterWhere(['like', 'err_code_des', $this->err_code_des])
            ->andFilterWhere(['like', 'payment_no', $this->payment_no])
            ->andFilterWhere(['like', 'payment_time', $this->payment_time]);

        return $dataProvider;
    }
}
