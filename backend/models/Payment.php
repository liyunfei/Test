<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "{{%payment}}".
 *
 * @property integer $id
 * @property string $mch_appid
 * @property string $mchid
 * @property string $device_info
 * @property string $nonce_str
 * @property string $sign
 * @property string $partner_trade_no
 * @property string $openid
 * @property string $check_name
 * @property string $re_user_name
 * @property integer $amount
 * @property string $desc
 * @property string $spbill_create_ip
 * @property string $return_code
 * @property string $return_msg
 * @property string $result_code
 * @property string $err_code
 * @property string $err_code_des
 * @property string $payment_no
 * @property string $payment_time
 */
class Payment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%payment}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['amount'], 'integer'],
            [['mch_appid'], 'string', 'max' => 20],
            [['mchid', 'device_info', 'nonce_str', 'sign', 'partner_trade_no', 'openid', 're_user_name', 'spbill_create_ip', 'err_code', 'payment_no', 'payment_time'], 'string', 'max' => 32],
            [['check_name'], 'string', 'max' => 12],
            [['desc', 'return_msg', 'err_code_des'], 'string', 'max' => 128],
            [['return_code', 'result_code'], 'string', 'max' => 16],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mch_appid' => 'Mch Appid',
            'mchid' => 'Mchid',
            'device_info' => 'Device Info',
            'nonce_str' => 'Nonce Str',
            'sign' => 'Sign',
            'partner_trade_no' => 'Partner Trade No',
            'openid' => 'Openid',
            'check_name' => 'Check Name',
            're_user_name' => 'Re User Name',
            'amount' => 'Amount',
            'desc' => 'Desc',
            'spbill_create_ip' => 'Spbill Create Ip',
            'return_code' => 'Return Code',
            'return_msg' => 'Return Msg',
            'result_code' => 'Result Code',
            'err_code' => 'Err Code',
            'err_code_des' => 'Err Code Des',
            'payment_no' => 'Payment No',
            'payment_time' => 'Payment Time',
        ];
    }
}
