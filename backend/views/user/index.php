<?php

use common\models\User;
use yii\helpers\Html;
use yii\grid\GridView;
use kartik\tabs\TabsX;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php echo Html::a(Yii::t('backend', 'Create {modelClass}', [
    'modelClass' => 'User',
]), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            'username',
            'email:email',
            [
                'class' => \common\grid\EnumColumn::className(),
                'attribute' => 'status',
                'enum' => User::getStatuses(),
                'filter' => User::getStatuses()
            ],
            'created_at:datetime',
            'logged_at:datetime',
            // 'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); 
    
//    echo TabsX::widget([
//    'position' => TabsX::POS_ABOVE,
//    'align' => TabsX::ALIGN_LEFT,
//    'items' => [
//        [
//            'label' => '获取用户已领取卡券',
//            'content' => 'Anim pariatur cliche...',
//            'active' => true
//        ],
//        [
//            'label' => 'Two',
//            'content' =>$this->render('view', [ 'model' => $model,]),
//            'headerOptions' => ['style'=>'font-weight:bold'],
//            'options' => ['id' => 'myveryownID'],
//        ],
//        [
//            'label' => 'Dropdown',
//            'items' => [
//                 [
//                     'label' => 'DropdownA',
//                     'content' => 'DropdownA, Anim pariatur cliche...',
//                 ],
//                 [
//                     'label' => 'DropdownB',
//                     'content' => 'DropdownB, Anim pariatur cliche...',
//                 ],
//            ],
//        ],
//    ],
//]);
    
    
    ?>

</div>
