<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Payment */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="payment-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->field($model, 'mch_appid')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'mchid')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'device_info')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'nonce_str')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'sign')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'partner_trade_no')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'openid')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'check_name')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 're_user_name')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'amount')->textInput() ?>

    <?php echo $form->field($model, 'desc')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'spbill_create_ip')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'return_code')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'return_msg')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'result_code')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'err_code')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'err_code_des')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'payment_no')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'payment_time')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?php echo Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>