<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\PaymentSearch */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="payment-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?php echo $form->field($model, 'id') ?>

    <?php echo $form->field($model, 'mch_appid') ?>

    <?php echo $form->field($model, 'mchid') ?>

    <?php echo $form->field($model, 'device_info') ?>

    <?php echo $form->field($model, 'nonce_str') ?>

    <?php // echo $form->field($model, 'sign') ?>

    <?php // echo $form->field($model, 'partner_trade_no') ?>

    <?php // echo $form->field($model, 'openid') ?>

    <?php // echo $form->field($model, 'check_name') ?>

    <?php // echo $form->field($model, 're_user_name') ?>

    <?php // echo $form->field($model, 'amount') ?>

    <?php // echo $form->field($model, 'desc') ?>

    <?php // echo $form->field($model, 'spbill_create_ip') ?>

    <?php // echo $form->field($model, 'return_code') ?>

    <?php // echo $form->field($model, 'return_msg') ?>

    <?php // echo $form->field($model, 'result_code') ?>

    <?php // echo $form->field($model, 'err_code') ?>

    <?php // echo $form->field($model, 'err_code_des') ?>

    <?php // echo $form->field($model, 'payment_no') ?>

    <?php // echo $form->field($model, 'payment_time') ?>

    <div class="form-group">
        <?php echo Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?php echo Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>