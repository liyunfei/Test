<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\PaymentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Payments';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php echo Html::a('Create Payment', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'mch_appid',
            'mchid',
            'device_info',
            'nonce_str',
            // 'sign',
            // 'partner_trade_no',
            // 'openid',
            // 'check_name',
            // 're_user_name',
            // 'amount',
            // 'desc',
            // 'spbill_create_ip',
            // 'return_code',
            // 'return_msg',
            // 'result_code',
            // 'err_code',
            // 'err_code_des',
            // 'payment_no',
            // 'payment_time',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>