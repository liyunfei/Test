<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Payment */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Payments', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-view">

    <p>
        <?php echo Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?php echo Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?php echo DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'mch_appid',
            'mchid',
            'device_info',
            'nonce_str',
            'sign',
            'partner_trade_no',
            'openid',
            'check_name',
            're_user_name',
            'amount',
            'desc',
            'spbill_create_ip',
            'return_code',
            'return_msg',
            'result_code',
            'err_code',
            'err_code_des',
            'payment_no',
            'payment_time',
        ],
    ]) ?>

</div>