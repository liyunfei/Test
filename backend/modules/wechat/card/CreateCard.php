<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace backend\modules\wechat\card;

use backend\modules\wechat\base\WechatBase;

/**
 * Description of CreateCard
 *
 * @author liyunfei
 */
class CreateCard extends WechatBase {

    public function GetColors() { 
        $result = $this->card->getColors();
        $colors = json_decode($result);
        $sate = is_object($colors) ? get_object_vars($colors) : $colors;
        $colors_all = [];
        if ($sate['errcode'] == 0 && $sate['errmsg'] == 'ok') {
            foreach ($sate['colors'] as $value) {
                $colors_list = is_object($value) ? get_object_vars($value) : $value;
                $colors_all[$colors_list['name']] = $colors_list['value'];
            }
        } else {
            return FALSE;
        }
        return $colors_all;
    }

    public function UploadLogo($path) {
        $result = $this->material->uploadImage($path);
        $logo = json_decode($result);
        return is_object($logo) ? get_object_vars($logo) : $logo;
    }

    public function Create($baseInfo, $cardType = null, $especial = null) {
        $result = $this->card->create($cardType, $baseInfo, $especial);
        print_r($result);
        $card = json_decode($result);
        $card_arr = is_object($card) ? get_object_vars($card) : $card;
        if ($card_arr['errcode'] == 0) {
            return $card_arr['card_id'];
        } else {
            return FALSE;
        }
    }

    public function CreateQRCode() {
        //领取单张卡券
        $cards = [
            'action_name' => 'QR_CARD',
            'expire_seconds' => 1800,
            'action_info' => [
                'card' => [
                    'card_id' => 'ps3zPjvGM5SRLAwLxHktaltnJJCQ',
                    'is_unique_code' => false,
                    'outer_id' => 1,
                ],
            ],
        ];
//        $result = $this->card->QRCode($cards);
        $code = '688703455070';
        $checkConsume = true;
        $cardId = 'ps3zPjvGM5SRLAwLxHktaltnJJCQ';
        $result = $this->card->getCode($code, $checkConsume, $cardId);

        print_r($result);
    }

    public function functionName() {
        $cardType = 'GROUPON';
        $baseInfo = [
            'logo_url' => 'http://mmbiz.qpic.cn/mmbiz/2aJY6aCPatSeibYAyy7yct9zJXL9WsNVL4JdkTbBr184gNWS6nibcA75Hia9CqxicsqjYiaw2xuxYZiaibkmORS2oovdg/0',
            'brand_name' => '测试商户造梦空间',
            'code_type' => 'CODE_TYPE_QRCODE',
            'title' => '测试',
            'sub_title' => '测试副标题',
            'color' => 'Color010',
            'notice' => '测试使用时请出示此券',
            'service_phone' => '15311931577',
            'description' => "测试不可与其他优惠同享\n如需团购券发票，请在消费时向商户提出\n店内均可使用，仅限堂食",
            'date_info' => [
                'type' => 'DATE_TYPE_FIX_TERM',
                'fixed_term' => 90, //表示自领取后多少天内有效，不支持填写0
                'fixed_begin_term' => 0, //表示自领取后多少天开始生效，领取后当天生效填写0。
            ],
            'sku' => [
                'quantity' => 10, //自定义code时设置库存为0
            ],
            'location_id_list' => ['461907340'], //获取门店位置poi_id，具备线下门店的商户为必填
            'get_limit' => 1,
//            'use_custom_code' => true, //自定义code时必须为true
//            'get_custom_code_mode' => 'GET_CUSTOM_CODE_MODE_DEPOSIT', //自定义code时设置
            'bind_openid' => false,
            'can_share' => true,
            'can_give_friend' => false,
            'center_title' => '顶部居中按钮',
            'center_sub_title' => '按钮下方的wording',
            'center_url' => 'http://www.qq.com',
            'custom_url_name' => '立即使用',
            'custom_url' => 'http://www.qq.com',
            'custom_url_sub_title' => '6个汉字tips',
            'promotion_url_name' => '更多优惠',
            'promotion_url' => 'http://www.qq.com',
            'source' => '造梦空间',
        ];
        $especial = [
            'deal_detail' => 'deal_detail',
        ];
        $result = $this->Create($baseInfo, $cardType, $especial);
        print_r($result);
    }

}
