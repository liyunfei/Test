<?php

namespace backend\modules\wechat\base;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use EasyWeChat\Foundation\Application;
use backend\modules\wechat\config\Config;

/**
 * Description of WechatBase
 *
 * @author liyunfei
 */
class WechatBase {

    public $card;
    public $material;
    public $poi;

    public function __construct() {
        $options = [
            'debug' => true,
            'app_id' => Config::appid,
            'secret' => Config::secret,
            'token' => 'easywechat',
            'log' => [
                'level' => 'debug',
                'file' => '/tmp/easywechat.log',
            ]
        ];
        $app = new Application($options);
        $this->card = $app->card;
        // 永久素材
        $this->material = $app->material;
        //门店
        $this->poi = $app->poi;
//        $card = $this->card;
      

    }

}
