<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace backend\modules\wechat;

/**
 * Description of PayMent
 *
 * @author liyunfei
 */
class PayMent {

    /**
     * 发起付款
     * @param type $merchantPayData
     * @return type
     */
    public function PayMent($merchantPayData) {
        $options = [
            'debug' => true,
            'app_id' => 'wxde12840eed5d4da0',
            'secret' => '13febe0d35a63d4b72c0575ea1e421a5',
            'token' => 'easywechat',
            'log' => [
                'level' => 'debug',
                'file' => '/tmp/easywechat.log',
            ],
            'payment' => [
                'merchant_id' => '1272907101',
                'key' => 'aisyIWHD836hsuIH6786JHWhuwguehf7',
                'cert_path' => '/home/liyunfei/apiclient_cert.pem',
                'key_path' => '/home/liyunfei/apiclient_key.pem',
            ],
        ];
        $app = new Application($options);
        $merchantPay = $app->merchant_pay;
        $result = $merchantPay->send($merchantPayData);
        return is_object($result) ? get_object_vars($result) : $result;
    }

    /**
     * 获取商户订单号
     * @param type $param
     */
    public function getPartner_trade_no() {
        $str = $this->getNonce_str(6);
        date_default_timezone_set('PRC');
        $date = date("YmdHis", time()); //YmdHis  20160825072205
        $Partner_trade_no = $date . $str;
        return $Partner_trade_no;
    }

    /**
     * 
     * @param type $len
     * @return type
     */
    public function getNonce_str($len) {
        $chars = 'abcdefghijklmnopqrstuvwxyz0123456789';
        mt_srand((double) microtime() * 1000000 * getmypid());
        $password = '';
        while (strlen($password) < $len) {
            $password.=substr($chars, (mt_rand() % strlen($chars)), 1);
        }
        return $password;
    }

}
