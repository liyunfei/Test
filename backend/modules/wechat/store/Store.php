<?php

namespace backend\modules\wechat\store;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use backend\modules\wechat\base\WechatBase;

/**
 * Description of Store
 *
 * @author liyunfei
 */
class Store extends WechatBase {

    /**
     * 上传门店图片
     * @param type $path
     * @return type url
     */
    public function UploadPhoto($path) {
        $result = $this->material->uploadImage($path);
        $logo = json_decode($result);
        return is_object($logo) ? get_object_vars($logo) : $logo;
    }

    public function Create() {
        $info = array(
            "sid" => "33788392",
            "business_name" => "麦当劳",
            "branch_name" => "艺苑路店",
            "province" => "广东省",
            "city" => "广州市",
            "district" => "海珠区",
            "address" => "艺苑路 11 号",
            "telephone" => "020-12345678",
            "categories" => array("美食,快餐小吃"),
            "offset_type" => 1,
            "longitude" => 115.32375,
            "latitude" => 25.097486,
            "photo_list" => array(
                array("photo_url" => "http://mmbiz.qpic.cn/mmbiz_jpg/AQFU1NTWANYGN5hp8IeeNJIkqqKH2EcYM07BIGsrxKThkF65dA8uvcThHKWtgjCRuoIYN9q45yCTThjGRuMGAQ/0?wx_fmt=jpeg"),
                array("photo_url" => "http://mmbiz.qpic.cn/mmbiz_jpg/AQFU1NTWANYGN5hp8IeeNJIkqqKH2EcYM07BIGsrxKThkF65dA8uvcThHKWtgjCRuoIYN9q45yCTThjGRuMGAQ/0?wx_fmt=jpeg"),
            ),
            "recommend" => "麦辣鸡腿堡套餐,麦乐鸡,全家桶",
            "special" => "免费 wifi,外卖服务",
            "introduction" => "麦当劳是全球大型跨国连锁餐厅,1940 年创立于美国,在世界上大约拥有 3  万间分店。主要售卖汉堡包,以及薯条、炸鸡、汽水、冰品、沙拉、水果等 快餐食品",
            "open_time" => "8:00-20:00",
            "avg_price" => 35,
        );
        $result = $this->poi->create($info); // true or exception
        print_r($result);
    }

}
